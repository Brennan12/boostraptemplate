﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BootstrapTemplate.Models
{
    public class MyDataClass
    {
        [Key]
        public int ID { get; set; }
        public string Username { get; set; }
    }
}
